from flask import Flask, jsonify,request
#from sklearn.externals import joblib
import joblib
import numpy as np
import pandas as pd
import json
from scipy import stats
from werkzeug.contrib.cache import SimpleCache
import sys
import datetime
from datetime import date
from datetime import datetime
from sqlalchemy import create_engine
from dateutil.relativedelta import relativedelta
from workalendar.africa import Madagascar
from datetime import timedelta
import time

# Initialize the app
app = Flask(__name__, instance_relative_config=True)

# Load the views
from bafla import view

# Load the config file
app.config.from_object('config')

cache = SimpleCache()

PRS_scoringAPlVersion = '0.0.0'
clf_firstTime = joblib.load('/var/www/BasicFlaskApp/bafla/model_firstTime.h5')
clf_repeated = joblib.load('/var/www/BasicFlaskApp/bafla/model_repeated.h5')

# clf=joblib.load(modelFile)
# GACFile=client.file('data://Lokyata/MDG/GACMatrixML_0321.csv').getFile().name
GAC_firstTime = pd.read_pickle('/var/www/BasicFlaskApp/bafla/GACMatrix_firstTime.pkl')
GAC_repeated = pd.read_pickle('/var/www/BasicFlaskApp/bafla/GACMatrix_repeated.pkl')

def unjsonify_pdpDict(pdpDict):
    for feature in pdpDict:
        for grid_val in pdpDict[feature]:
            pdpDict[feature][grid_val] = np.float(pdpDict[feature][grid_val])
    return pdpDict

# pdpDictFile=client.file('data://Lokyata/MDG/PDP_MDG_RF_0102Data_0205Model_0419.json').getFile().name
with open('/var/www/BasicFlaskApp/bafla/pdpDictionary_firstTime.json') as json_file:
    pdpDict_firstTime = json.load(json_file)
pdpDict_firstTime = unjsonify_pdpDict(pdpDict_firstTime)
# FIFile=client.file('data://Lokyata/MDG/FI_MDG_RF_0102Data_0205Model_0419.csv').getFile().name
FI_firstTime = pd.read_csv('/var/www/BasicFlaskApp/bafla/featureImportance_firstTime.csv')
with open('/var/www/BasicFlaskApp/bafla/pdpDictionary_repeated.json') as json_file:
    pdpDict_repeated = json.load(json_file)
pdpDict_repeated = unjsonify_pdpDict(pdpDict_repeated)
# FIFile=client.file('data://Lokyata/MDG/FI_MDG_RF_0102Data_0205Model_0419.csv').getFile().name
FI_repeated = pd.read_csv('/var/www/BasicFlaskApp/bafla/featureImportance_repeated.csv')



def band(bandValues, x):
    """
    Band values to ceiling of range
    Use when bands are unevenly spaced
    If bands evenly spaced, just use np.ceil(x*2)/2
    """
    a =np.array(bandValues)
    b = a - x
    #c[0] is first element where x<band value
    c = b[b >= 0]
    #if x> all band values, take last band value
    if len(c) == 0:
        return a[len(a) - 1]
    #otherwise take index of first time x<=band value
    else:
        return a[list(b).index(c[0])]


def bandClosest(bandValues, myNumber):
    """
    Round myNumber to closest value in bandValue. If exactly in between 2 numbers, will round down.
    """
    return min(bandValues, key=lambda x: abs(x - myNumber))


def calcGA(GAC,PD):
    """"
    Calculate GA at run time for a single customer X
    GAC: Static GAC table with two columns - PD_Bands and GA_Bands
    PD: PD value for customer X
    Example:
    calcGA(GAC,0.0080)
    """
    #round PD to nearest PD band
    a = band(GAC.PD_Bands,PD)
    #lookup rounded PD band in static GAC table. Return corresponding GA
    return GAC[GAC.PD_Bands == a].GA_Bands.values[0]


def calc_GA_haircuts(current_loan_cycle, GA_ML):
    if current_loan_cycle == 1 or current_loan_cycle >= 6:
        return GA_ML
    if current_loan_cycle == 2 or current_loan_cycle == 3:
        return GA_ML * 0.8
    if current_loan_cycle == 4 or current_loan_cycle == 5:
        return GA_ML * 0.9


def calc_GA_policyLayer(loanDel_Over_loanCyc, prevAmout, GA_haircuts):
    if loanDel_Over_loanCyc <= 0.1:
        return max(prevAmout, GA_haircuts)
    return GA_haircuts


def calc_GA_fraudPrevention(HIS_maxAmount, GA_policyLayer):
    buffer_amount = 100000
    return min(HIS_maxAmount+buffer_amount, GA_policyLayer)


def PDP_lookup(pdpDict, varName, value, num_decimals_round=5, percentile=False):
    """
    Helper function for decisionCode()
    Compare value to list of pdp grid values and find closest grid value.
    Take corresponding pdp for this grid value.
    Compare pdp value to list of all pdp values and return percentile of this pdp value in the list of pdp values.
    :param pdpDict: output by PDP_Dict() - nested dictionary of {varNameString:{pdpDict_varName} where pdpDict_varName is a dictionary of {grid_value: pdp_value}
    :param varName: variable name string to use as key in pdpDict
    :param value: value of variable to compare to grid values
    :param num_decimals_round: number of decimal points to round the grid values to (see docstring for PDP_Dict)
    :param percentile: whether return a float percentile pdp value
    :returns if percentile is True then return a float percentile pdp value, otherwise the raw pdp banded values will be returned
    """
    pdpDict_var=pdpDict[varName]
    # We do the conversion here because if we store pdpDict in cache (JSON format), all the keys will be strings, there is no way to check the original type.
    # We need to make sure all features are factorized properly (which should happen after feature engineering step) before we generate pdpDict
    gridVals=pd.Series(list(pdpDict_var.keys())).apply(lambda x: float(x))
    pdpVals=list(pdpDict_var.values())
    pdp_banded_value = pdpDict_var[str(bandClosest(gridVals,value))]
    if percentile == True:
        return stats.percentileofscore(pdpVals, pdp_banded_value)
    return pdp_banded_value


def decisionCode(pdpDict, FI, inputData):
    """
    Pass in single row (loan) and return decision code category which contributes highest to loan risk.
    inputData needs to be converted from JSON format to a data frame before passing into this function, and inputData should be only for one single request
    :param pdpDict: output by PDP_Dict() - nested dictionary of {varNameString:{pdpDict_varName} where pdpDict_varName is a dictionary of {grid_value: pdp_value}
    :param FI: output of featureImportance() - dataframe with cols variable, importance, category
    :param inputData: dataframe with single row (one request) and a column for each feature
    :returns string category name
    """
    for col in inputData.columns:
        if inputData[col].apply(lambda x: isinstance(x, (int, float))).all() == False:
            raise TypeError(f'column \'{col}\' is not numeric, you can do the following to make sure inputData features have correct types:\ninputData=inputData.apply(lambda x: pd.to_numeric(x))')
    df=inputData.apply(lambda x: PDP_lookup(pdpDict,x.name,x[0],percentile=True)).reset_index() # this X[0] trick can convert the data frame from column-wise to row-wise. However, if we want to do batch calculation inside this function rather than calling this function multiple times in the future, we might need to do a df=df.T.
    df.columns='variable impact'.split()
    df2=FI.merge(df,on='variable',how='inner')
    df2['weighted_impact']=df2.importance*df2.impact
    #Return category which has highest sum of prediction values
    df3=df2.groupby('category')['weighted_impact'].sum().reset_index()
    maxCategory=df3[df3.weighted_impact==max(df3.weighted_impact)].category.values[0]
    decCode_dict = dict(zip(df3['category'],df3['weighted_impact']))
    return maxCategory, decCode_dict


def sub_scores(pdpDict,FI,inputData):
    """
    Pass in single row (loan) and return all sub-scores for this loan.
    inputData needs to be converted from JSON format to a data frame before passing into this function, and inputData should be only for one single request
    :param pdpDict: output by PDP_Dict() - nested dictionary of {varNameString:{pdpDict_varName} where pdpDict_varName is a dictionary of {grid_value: pdp_value}
    :param FI: output of featureImportance() - dataframe with cols variable, importance, category
    :param inputData: dataframe with single row (one request) and a column for each feature
    :returns a pandas df that have all normalized weighted pdp and sub-scores information for this loan (at category level)
    """
    for col in inputData.columns:
        if inputData[col].apply(lambda x: isinstance(x, (int, float))).all() == False:
            raise TypeError(f'column \'{col}\' is not numeric, you can do the following to make sure inputData features have correct types:\ninputData=inputData.apply(lambda x: pd.to_numeric(x))')
    df=inputData.apply(lambda x: PDP_lookup(pdpDict,x.name,x[0])).reset_index() # this X[0] trick can convert the data frame from column-wise to row-wise. However, if we want to do batch calculation inside this function rather than calling this function multiple times in the future, we might need to do a df=df.T.
    df.columns='variable pdp'.split()
    df2=FI.merge(df,on='variable',how='inner')
    df2['weighted_pdp']=df2.importance*df2.pdp
    df3=df2.groupby('category')[['weighted_pdp','importance']].sum().reset_index()
    # normalize values so that sum(importance) for each category sums to 1
    df3['normalized_weighted_pdp'] = df3.weighted_pdp/df3.importance
    df3['sub_scores'] = df3['normalized_weighted_pdp'].apply(lambda x: round(1000*(1-x)))
    # return df3[['category', 'normalized_weighted_pdp', 'sub_scores']]
    subScore_dict = dict(zip(df3['category'],df3['sub_scores']))
    return subScore_dict


def getGAFromML(input):
    print("creating new instance")
    X = pd.DataFrame(input, index=[0])    
    current_loan_cycle = X['current_loan_cycle'][0]
    loanDel_Over_loanCyc = X['loanDel_Over_loanCyc'][0]
    prevAmout = X['prevAmout'][0]
    HIS_maxAmount = X['HIS_maxAmount'][0]
    MNO_postpaid_flag = X['MNO_postpaid_flag'][0]
    
    if current_loan_cycle == 1:
        X = X.loc[:, ['WAL_seniority_B20', 'MNO_seniority_B20',
                      'COUNT<MNO_recharge_LBM12>_B20', 'SUM<MNO_recharge_LBM12>_B20',
                      '<SUM<MNO_voiceRecharge>_LBM12_over_SUM<MNO_recharge>_LBM12>_B20',
                      '<SUM<MNO_eRecharge>_LBM12_over_SUM<MNO_recharge>_LBM12>_B20', 'SUM<WAL_cashin_LBM12>_B20',
                      '<SUM<MNO_recharge>_LBM12_over_SUM<WAL_cashin>_LBM12>_B20', 'SUM<WAL_cashflow_LBM12>_B20',
                      '<SUM<WAL_cashflow>_LBM12_over_SUM<WAL_cashin>_LBM12>_B20',
                      'COUNT<WAL_cashTransactions_LBM12>_B20',
                      '<COUNT<MNO_recharge>_LBM12_over_COUNT<WAL_cashTransactions>_LBM12>_B20',
                      'DEM_age']]
        t0 = time.time()
        
        #---------
        # IMPORTANT: Addition validation protection layer, can remove this if validation is done upstream
        if pd.isna(X.iloc[0,:]).any():
            raise ValueError("Found missing value in input")

        incorrect_dataType = X.apply(lambda column: column.apply(lambda value: np.nan if type(value) != float and type(value) != int and type(value) != bool else value))
        if pd.isna(incorrect_dataType.iloc[0,:]).any():
            raise ValueError("Found incorrect data type in input, input needs to be one of the following: [float, int, bool]")
        #---------
        
        pBad = clf_firstTime.predict_proba(X)[0][1]
        t1 = time.time()
        PRS_pBad_timeElapsed = t1 - t0
        Score = int((1 - pBad) * 1000)

        t0 = time.time()
        GA_ML = calcGA(GAC_firstTime, pBad)
        GA_haircuts = GA_ML
        GA_policyLayer = GA_ML
        GA_fraudPrevention = GA_ML
        GA = GA_ML
        t1 = time.time()
        PRS_GA_timeElapsed = t1-t0

        t0 = time.time()
        DC, decCode_dict = decisionCode(pdpDict_firstTime, FI_firstTime, X)
        t1 = time.time()
        PRS_decCode_timeElapsed = t1 - t0

        if GA == 0 or GA == 0.0:
            Approved = 0
        else:
            Approved = 1
            DC = "Approved"

        subScore_dict = sub_scores(pdpDict_firstTime, FI_firstTime, X)

    if current_loan_cycle > 1:
        X = X.loc[:, ['WAL_seniority_B20', 'MNO_seniority_B20',
                      'COUNT<MNO_recharge_LBM12>_B20', 'SUM<MNO_recharge_LBM12>_B20',
                      '<SUM<MNO_voiceRecharge>_LBM12_over_SUM<MNO_recharge>_LBM12>_B20',
                      '<SUM<MNO_eRecharge>_LBM12_over_SUM<MNO_recharge>_LBM12>_B20', 'SUM<WAL_cashin_LBM12>_B20',
                      '<SUM<MNO_recharge>_LBM12_over_SUM<WAL_cashin>_LBM12>_B20', 'SUM<WAL_cashflow_LBM12>_B20',
                      '<SUM<WAL_cashflow>_LBM12_over_SUM<WAL_cashin>_LBM12>_B20',
                      'COUNT<WAL_cashTransactions_LBM12>_B20',
                      '<COUNT<MNO_recharge>_LBM12_over_COUNT<WAL_cashTransactions>_LBM12>_B20', 'DEM_gender',
                      'DEM_maritalStatus',
                      'DEM_rural_urban', 'COUNT<LEN_savingsCashin>', 'MEAN<LEN_savingsBalance>',
                      'SUM<LEN_savingsCashin>', 'current_loan_cycle',
                      'COUNT<prevLoans$FAC_loanID>_LBM3', 'MAX<prevLoans$FAC_DPD>_LBM3',
                      'MAX<prevLoans$FAC_DPD>', 'MEAN<prevLoans$FAC_DPD>_LBM3', 'HIS_m2H1', 'HIS_m2H21', 'HIS_m2H22',
                      'HIS_m2H3',
                      'DEM_age', 'daysAccountOpen', 'hour', 'weekday', 'week_of_the_month', 'isHoliday', 'DEM_region_conversion']]
        t0 = time.time()
        
        #---------
        # IMPORTANT: Addition validation protection layer, can remove this if validation is done upstream
        if pd.isna(X.iloc[0,:]).any():
            raise ValueError("Found missing value in input")

        incorrect_dataType = X.apply(lambda column: column.apply(lambda value: np.nan if type(value) != float and type(value) != int and type(value) != bool else value))
        if pd.isna(incorrect_dataType.iloc[0,:]).any():
            raise ValueError("Found incorrect data type in input, input needs to be one of the following: [float, int, bool]")
        #---------
        
        pBad = clf_repeated.predict_proba(X)[0][1]
        t1 = time.time()
        PRS_pBad_timeElapsed = t1-t0
        Score = int((1 - pBad) * 1000)

        t0 = time.time()
        GA_ML = calcGA(GAC_repeated, pBad)
        GA_haircuts = calc_GA_haircuts(current_loan_cycle, GA_ML)
        GA_policyLayer = calc_GA_policyLayer(loanDel_Over_loanCyc, prevAmout, GA_haircuts)
        GA_fraudPrevention = calc_GA_fraudPrevention(HIS_maxAmount, GA_policyLayer)
        GA = GA_fraudPrevention
        t1 = time.time()
        PRS_GA_timeElapsed = t1-t0

        t0 = time.time()
        DC, decCode_dict = decisionCode(pdpDict_repeated, FI_repeated, X)
        t1 = time.time()
        PRS_decCode_timeElapsed = t1 - t0

        if GA == 0 or GA == 0.0:
            Approved = 0
        else:
            Approved = 1
            DC = "Approved"

        subScore_dict = sub_scores(pdpDict_repeated, FI_repeated, X)

    decCode_dict = str(decCode_dict)
    subScore_dict = str(subScore_dict)
    response = {'PRS_pBad': pBad, 'PRS_credit_score': Score, 'PRS_grantableAmount_GAC': GA_ML, 'PRS_grantableAmount_haircuts': GA_haircuts, 'PRS_grantableAmount_policyLayer': GA_policyLayer,
                'PRS_grantableAmount_fraudPrevention': GA_fraudPrevention,  'PRS_grantableAmount': GA, 'PRS_loanApproved_flag': Approved, 'PRS_rejectionReason': DC,
                'PRS_decCode_dict': decCode_dict, 'subScore_dict': subScore_dict,
                'PRS_GA_timeElapsed': PRS_GA_timeElapsed, 'PRS_pBad_timeElapsed': PRS_pBad_timeElapsed, 'PRS_decCode_timeElapsed': PRS_decCode_timeElapsed,
                'PRS_scoringAPlVersion':PRS_scoringAPlVersion}

    return response


@app.route('/api/v1/model/evaluate/loan', methods=['POST'])
def evaluateLoan():
    print(request)
    if not request.json or not 'WAL_seniority_B20' in request.json:
        abort(400)
    input = request.get_json()
    # requestIdentifier = str(request.json['orangeMoneySeniority']) + "_" + str(
    #     request.json['mobileAccountSeniority']) + "_" + str(request.json['totalNumOfRecharges']) + "_" + str(
    #     request.json['totalValOfRecharges']) + "_" + str(request.json['voiceOverTotalRecharges']) + "_" + str(
    #     request.json['eRechargeOverTotalRecharges']) + "_" + str(request.json['totalValOfCashins']) + "_" + str(
    #     request.json['totalRechargesOverTotalCashins']) + "_" + str(request.json['totalCashFlow']) + "_" + str(
    #     request.json['totalCashFlowOverTotalCashins']) + "_" + str(
    #     request.json['totalNumOfCashTransactions']) + "_" + str(request.json['tRchrgNumOverTcashTransNum']) + "_" + str(
    #     request.json['gender']) + "_" + str(request.json['maritalStatus']) + "_" + str(
    #     request.json['location']) + "_" + str(request.json['m3_numberOfCashins']) + "_" + str(
    #     request.json['averageBalance']) + "_" + str(request.json['m3_totalCashins']) + "_" + str(
    #     request.json['numberofSuccessfulLoans']) + "_" + str(request.json['age']) + "_" + str(
    #     request.json['daysAccountOpen'])
    requestIdentifier = str(request.json["WAL_seniority_B20"]) + "_" + str(request.json["MNO_seniority_B20"]) + "_" + str(
        request.json["COUNT<MNO_recharge_LBM12>_B20"]) + "_" + str(
        request.json["SUM<MNO_recharge_LBM12>_B20"]) + "_" + str(
        request.json["<SUM<MNO_voiceRecharge>_LBM12_over_SUM<MNO_recharge>_LBM12>_B20"]) + "_" + str(
        request.json["<SUM<MNO_eRecharge>_LBM12_over_SUM<MNO_recharge>_LBM12>_B20"]) + "_" + str(
        request.json["SUM<WAL_cashin_LBM12>_B20"]) + "_" + str(
        request.json["<SUM<MNO_recharge>_LBM12_over_SUM<WAL_cashin>_LBM12>_B20"]) + "_" + str(
        request.json["SUM<WAL_cashflow_LBM12>_B20"]) + "_" + str(
        request.json["<SUM<WAL_cashflow>_LBM12_over_SUM<WAL_cashin>_LBM12>_B20"]) + "_" + str(
        request.json["COUNT<WAL_cashTransactions_LBM12>_B20"]) + "_" + str(
        request.json["<COUNT<MNO_recharge>_LBM12_over_COUNT<WAL_cashTransactions>_LBM12>_B20"]) + "_" + str(
        request.json["DEM_gender"]) + "_" + str(request.json["DEM_maritalStatus"]) + "_" + str(
        request.json["DEM_rural_urban"]) + "_" + str(request.json["COUNT<LEN_savingsCashin>"]) + "_" + str(
        request.json["MEAN<LEN_savingsBalance>"]) + "_" + str(request.json["SUM<LEN_savingsCashin>"]) + "_" + str(
        request.json["current_loan_cycle"]) + "_" + str(request.json["COUNT<prevLoans$FAC_loanID>_LBM3"]) + "_" + str(
        request.json["MAX<prevLoans$FAC_DPD>_LBM3"]) + "_" + str(request.json["MAX<prevLoans$FAC_DPD>"]) + "_" + str(
        request.json["MEAN<prevLoans$FAC_DPD>_LBM3"]) + "_" + str(request.json["HIS_m2H1"]) + "_" + str(
        request.json["HIS_m2H21"]) + "_" + str(request.json["HIS_m2H22"]) + "_" + str(
        request.json["HIS_m2H3"]) + "_" + str(request.json["DEM_age"]) + "_" + str(
        request.json["daysAccountOpen"]) + "_" + str(request.json["hour"]) + "_" + str(
        request.json["weekday"]) + "_" + str(request.json["week_of_the_month"]) + "_" + str(
        request.json["isHoliday"]) + "_" + str(request.json["DEM_region_conversion"])
    response = cache.get(requestIdentifier)
    print(cache)
    print(requestIdentifier)
    print(response)
    if response is None:
        response = getGAFromML(input)
        cache.set(requestIdentifier, response, timeout=5 * 60)

    response = app.response_class(response=json.dumps(response), status=200, mimetype='application/json')
    return response


    
@app.route('/Helloworld', methods=['GET'])
def hello():
    return 'Hello, World!'
	
#@app.route('/GetCreditHistoryVariables', methods=['POST'])
#def GetCreditHistoryVariables():
#    # print(request)
#    # if not request.json or not 'title' in request.json:
#    #    abort(400)

#    data = request.get_json(force=True)
#    print("anoop")
#    print(data)
#    lstData = pd.DataFrame.from_dict(data, orient='columns')
#    lstData["CreatedDate"] = pd.to_datetime(lstData["CreatedDate"])
#    lstData["PaymentReceivedDate"] = pd.to_datetime(lstData["PaymentReceivedDate"])
#    currentLoanCycle = len(lstData) + 1

#    print(lstData)
#    print(currentLoanCycle)

#    M2_H1, m2H1_numberOfRepaidLoans = Get_M2_H1(lstData, currentLoanCycle)
#    M2_H21, m2H21_maxPDD = Get_M2_H21(lstData, currentLoanCycle)
#    M2_H22, m2H22_maxPDD = Get_M2_H22(lstData, currentLoanCycle)
#    M2_H3, m2H3_averagePDD = Get_M2_H3(lstData, currentLoanCycle)
#    prevAmount = get_prevAmount(lstData, currentLoanCycle)
#    HIS_maxAmount = get_HIS_maxAmount(lstData, currentLoanCycle)
#    loanDel_Over_loanCyc = get_loanDel_Over_loanCyc(lstData, currentLoanCycle)

#    print(M2_H1)
#    print(M2_H21)
#    print(M2_H22)
#    print(M2_H3)

#    outputDict = {}
#    outputDict['current_loan_cycle'] = currentLoanCycle
#    outputDict['COUNT<prevLoans$FAC_loanID>_LBM3'] = m2H1_numberOfRepaidLoans
#    outputDict['MAX<prevLoans$FAC_DPD>_LBM3'] = m2H21_maxPDD
#    outputDict['MAX<prevLoans$FAC_DPD>'] = int(m2H22_maxPDD)
#    outputDict['MEAN<prevLoans$FAC_DPD>_LBM3'] = m2H3_averagePDD
#    outputDict["HIS_m2H1"] = M2_H1
#    outputDict["HIS_m2H21"] = M2_H21
#    outputDict["HIS_m2H22"] = M2_H22
#    outputDict["HIS_m2H3"] = M2_H3
#    outputDict["prevAmount"] = prevAmount
#    outputDict["HIS_maxAmount"] = HIS_maxAmount
#    outputDict["loanDel_Over_loanCyc"] = loanDel_Over_loanCyc



#    print(outputDict)

#    response = app.response_class(response=json.dumps(outputDict), status=200, mimetype='application/json')
#    print(response)
#    return response


def Get_M2_H1(lstData, currentLoanCycle):
    if (currentLoanCycle == 1):
        return 1, 0

    numberOfRepaidLoans = 0
    threeMonthsStartDate = pd.Timestamp(date.today()) + relativedelta(months=-3)

    if (lstData is not None):
        numberOfRepaidLoans = len(lstData[lstData['CreatedDate'] >= threeMonthsStartDate].index)
    else:
        numberOfRepaidLoans = 0



    if (currentLoanCycle == 2 and numberOfRepaidLoans == 0):
        return 1.5, numberOfRepaidLoans

    if (currentLoanCycle == 2 and numberOfRepaidLoans == 1):
        return 3, numberOfRepaidLoans

    if (currentLoanCycle == 3 and numberOfRepaidLoans == 0):
        return 2, numberOfRepaidLoans

    if (currentLoanCycle == 3 and numberOfRepaidLoans == 1):
        return 4, numberOfRepaidLoans

    if (currentLoanCycle == 3 and numberOfRepaidLoans == 2):
        return 6, numberOfRepaidLoans

    if (currentLoanCycle >= 4 and numberOfRepaidLoans == 0):
        return 3.5, numberOfRepaidLoans

    if (currentLoanCycle >= 4 and numberOfRepaidLoans == 1):
        return 5.5, numberOfRepaidLoans

    if (currentLoanCycle >= 4 and numberOfRepaidLoans == 2):
        return 8, numberOfRepaidLoans

    if (currentLoanCycle >= 4 and numberOfRepaidLoans >= 3):
        return 9, numberOfRepaidLoans

    return -15, numberOfRepaidLoans


def Get_M2_H21(lstData, currentLoanCycle):
    maxPDD = 0
    if (currentLoanCycle == 1):
        return 1, maxPDD

    threeMonthsStartDate = pd.Timestamp(date.today()) + relativedelta(months=-3)
    lstFilteredData = None

    if (lstData is not None):
        lstFilteredData = lstData[lstData['CreatedDate'] >= threeMonthsStartDate]
    else:
        return 1, maxPDD
    # print(lstFilteredData)
    if (lstFilteredData is not None and (len(lstFilteredData.index)) > 0):

        maxPDD = lstFilteredData['PDD'].max()


    else:
        return 1, maxPDD

    if (maxPDD <= 0):
        return 7, maxPDD

    if (maxPDD <= 2):
        return 5, maxPDD

    if (maxPDD <= 5):
        return 2, maxPDD

    if (maxPDD <= 10):
        return -1, maxPDD

    if (maxPDD <= 15):
        return -2, maxPDD

    if (maxPDD <= 30):
        return -3, maxPDD

    if (maxPDD > 30):
        return -3, maxPDD

    return -15, maxPDD


def Get_M2_H22(lstData, currentLoanCycle):
    maxPDD = 0
    if (currentLoanCycle == 1):
        return 1, maxPDD

    if (lstData is not None and (len(lstData.index)) > 0):

        maxPDD = lstData['PDD'].max()


    else:
        return 1, maxPDD

    if (maxPDD <= 0):
        return 6, maxPDD

    if (maxPDD <= 2):
        return 4, maxPDD

    if (maxPDD <= 5):
        return 2, maxPDD

    if (maxPDD <= 10):
        return -1, maxPDD

    if (maxPDD <= 15):
        return -2, maxPDD

    if (maxPDD <= 30):
        return -2, maxPDD

    if (maxPDD > 30):
        return -2, maxPDD

    return -15, maxPDD


def Get_M2_H3(lstData, currentLoanCycle):
    averagePDD = 0
    if (currentLoanCycle == 1):
        return 1, averagePDD

    threeMonthsStartDate = pd.Timestamp(date.today()) + relativedelta(months=-3)
    lstFilteredData = None

    if (lstData is not None):
        lstFilteredData = lstData[lstData['CreatedDate'] >= threeMonthsStartDate]
    else:
        return 1, averagePDD

    if (lstFilteredData is not None and len(lstFilteredData.index) > 0):

        averagePDD = lstFilteredData['PDD'].mean()

    else:
        return 1, averagePDD
    # print(lstFilteredData)

    if (averagePDD > 3):
        return -1, averagePDD

    if (averagePDD >= 0):
        return 0, averagePDD

    if (averagePDD >= -5):
        return 3, averagePDD

    if (averagePDD >= -10):
        return 5, averagePDD

    if (averagePDD >= -15):
        return 5, averagePDD

    if (averagePDD < -15):
        return 5, averagePDD

    return -15, averagePDD

def get_prevAmount(lstData, currentLoanCycle):
    if (currentLoanCycle == 1):
        return 0
    else:
        return lstData.loc[lstData['CreatedDate'] == lstData['CreatedDate'].max(), 'LoanAmount'].values[0]

def get_HIS_maxAmount(lstData, currentLoanCycle):
    if (currentLoanCycle == 1):
        return 0
    else:
        return lstData['LoanAmount'].max()

def get_loanDel_Over_loanCyc(lstData, currentLoanCycle):
    if (currentLoanCycle == 1):
        return 0
    else:
        return len(lstData[lstData['PDD'] > 15])/len(lstData)

	
def get_hour(FAC_API_requestDate):
    date_time = FAC_API_requestDate
    hour = date_time.hour
    return hour

# Get day of the week feature
def get_weekday(FAC_API_requestDate):
    date = FAC_API_requestDate.date()
    weekday = date.weekday()+1
    return weekday

# Get week of the month feature (first day of first week starts from first day of the month)
def get_week_of_the_month(FAC_API_requestDate):
    # This get_week function is assuming first day of first week starting on first day of the month
    date = FAC_API_requestDate.date()
    day_of_month = date.day
    week_of_the_month = (day_of_month - 1) // 7 + 1
    return week_of_the_month

# Whether request date is a public holiday in Madagascar

def get_isHoliday(FAC_API_requestDate):
    """
    Currently, holidays in this workalendar package might not be a exhausted list
    ref: https://www.timeanddate.com/holidays/madagascar/
        [(datetime.date(2019, 1, 1), 'New year'),
     (datetime.date(2019, 3, 29), "Martyrs' Day"),
     (datetime.date(2019, 4, 22), 'Easter Monday'),
     (datetime.date(2019, 5, 1), 'Labour Day'),
     (datetime.date(2019, 5, 30), 'Ascension Thursday'),
     (datetime.date(2019, 6, 10), 'Whit Monday'),
     (datetime.date(2019, 6, 26), 'Independence Day'),
     (datetime.date(2019, 8, 15), 'Assumption of Mary to Heaven'),
     (datetime.date(2019, 11, 1), 'All Saints Day'),
     (datetime.date(2019, 12, 25), 'Christmas Day')]
    """
    cal = Madagascar()
    date = FAC_API_requestDate.date()
    year = date.year
    MDG_holidays = cal.holidays_set(year)
    return True if date in MDG_holidays else False


# Get age based on dob info
def get_age(FAC_API_requestDate,DEM_dob):
    request_date_time = FAC_API_requestDate
    dob_time = DEM_dob
    age = (request_date_time - dob_time).days/365
    return age

def get_daysAccountOpen(FAC_API_requestDate,LEN_accountOpenDate):
    request_date_time = FAC_API_requestDate
    account_open_date_time = LEN_accountOpenDate
    daysAccountOpen = (request_date_time - account_open_date_time).days
    return daysAccountOpen



@app.route('/api/v1/calculate/features', methods=['POST'])
# Calculate all features for one single input
def get_all_request_features():

    DEM_dob = request.args.get('DEM_dob')
    FAC_API_requestDate = request.args.get('FAC_API_requestDate')
    DEM_region = request.args.get('DEM_region')
    LEN_accountOpenDate = request.args.get('LEN_accountOpenDate')

    DEM_dob = datetime.strptime(DEM_dob,"%Y-%m-%d")
    FAC_API_requestDate =  datetime.strptime(FAC_API_requestDate.replace("_"," "),"%Y-%m-%d %H:%M:%S.%f")
    LEN_accountOpenDate = datetime.strptime(LEN_accountOpenDate,"%Y-%m-%d")
    
    #print(DEM_dob)
    #print(FAC_API_requestDate)
    #print(DEM_region)
    #print(LEN_accountOpenDate)

    # lstData = pd.DataFrame.from_dict(data, orient='columns')

    # inputData is a dictionary read from a json file or a flask request.get_json, assuming that this is always true
    # inputData = pd.DataFrame(inputData, index=[0])

    # Get this dictionary by analyzing badRate vs region in train data
    region_badRateRank_dic = {'Atsimo Atsinanana': 0,
                              'Sava': 1,
                              'Menabe': 2,
                              'Diana': 3,
                              'Bongolava': 4,
                              'Analanjirofo': 5,
                              'Boeny': 6,
                              'Atsimo Andrefana': 7,
                              'Melaky': 8,
                              'Sofia': 9,
                              'Androy': 10,
                              'Alaotra Mangoro': 11,
                              'Betsiboka': 12,
                              'Anosy': 13,
                              'Haute Matsiatra': 14,
                              'Vatovavy Fitovinany': 15,
                              'Atsinanana': 16,
                              'Vakinankaratra': 17,
                              "Amoron'i Mania": 18,
                              'Analamanga': 19,
                              'Ihorombe': 20,
                              'Itasy': 21}

    # Check if DEM_region is in train_data. If new (unseen in train_data), set to highest pBad region
    if DEM_region not in region_badRateRank_dic:
        DEM_region = 'Itasy'

    age = get_age(FAC_API_requestDate,DEM_dob)
    daysAccountOpen = get_daysAccountOpen(FAC_API_requestDate,LEN_accountOpenDate)
    hour = get_hour(FAC_API_requestDate)
    weekday = get_weekday(FAC_API_requestDate)
    week_of_the_month = get_week_of_the_month(FAC_API_requestDate)
    isHoliday = get_isHoliday(FAC_API_requestDate)

    data = request.get_json(force=True)
    #print(data)

    lstData = pd.DataFrame.from_dict(data, orient='columns')

    if (lstData is not None and (len(lstData.index)) > 0):
        lstData["CreatedDate"] = pd.to_datetime(lstData["CreatedDate"])
        lstData["PaymentReceivedDate"] = pd.to_datetime(lstData["PaymentReceivedDate"])
        currentLoanCycle = len(lstData) + 1
    else:
        currentLoanCycle = 1

    #print(lstData)
    #print(currentLoanCycle)

    M2_H1, m2H1_numberOfRepaidLoans = Get_M2_H1(lstData, currentLoanCycle)
    M2_H21, m2H21_maxPDD = Get_M2_H21(lstData, currentLoanCycle)
    M2_H22, m2H22_maxPDD = Get_M2_H22(lstData, currentLoanCycle)
    M2_H3, m2H3_averagePDD = Get_M2_H3(lstData, currentLoanCycle)
    prevAmount = get_prevAmount(lstData, currentLoanCycle)
    HIS_maxAmount = get_HIS_maxAmount(lstData, currentLoanCycle)
    loanDel_Over_loanCyc = get_loanDel_Over_loanCyc(lstData, currentLoanCycle)

    #print(M2_H1)
    #print(M2_H21)
    #print(M2_H22)
    #print(M2_H3)

    outputDict = {}
    outputDict["DEM_age"] = age
    outputDict["daysAccountOpen"] = daysAccountOpen
    outputDict["hour"] = hour
    outputDict["weekday"] = weekday
    outputDict["week_of_the_month"] = week_of_the_month
    outputDict["isHoliday"] = isHoliday
    outputDict['DEM_region_conversion'] = region_badRateRank_dic[DEM_region]
    outputDict['current_loan_cycle'] = currentLoanCycle
    outputDict['COUNT<prevLoans$FAC_loanID>_LBM3'] = m2H1_numberOfRepaidLoans
    outputDict['MAX<prevLoans$FAC_DPD>_LBM3'] = m2H21_maxPDD
    outputDict['MAX<prevLoans$FAC_DPD>'] = int(m2H22_maxPDD)
    outputDict['MEAN<prevLoans$FAC_DPD>_LBM3'] = m2H3_averagePDD
    outputDict["HIS_m2H1"] = M2_H1
    outputDict["HIS_m2H21"] = M2_H21
    outputDict["HIS_m2H22"] = M2_H22
    outputDict["HIS_m2H3"] = M2_H3
    outputDict["prevAmount"] = prevAmount
    outputDict["HIS_maxAmount"] = HIS_maxAmount
    outputDict["loanDel_Over_loanCyc"] = loanDel_Over_loanCyc

    response = app.response_class(response=json.dumps(outputDict), status=200, mimetype='application/json')
    return response


	
